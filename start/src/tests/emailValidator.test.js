const chai = require('chai');
const { expect } = chai;
const sinon = require("sinon");
const { validate, validateAsync, validateWithThrow, validateWithLog } = require("../email-validator.js");



describe('first test', () => {
    it('should return 2', () => {
        expect(2).to.equal(2);
    })
});

describe('validate function', () => {
    it('should return true when post service is gmail.com', () => {
        const expected = true;
        const actual = validate('nika@gmail.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return true when post service is yandex.ru', () => {
        const expected = true;
        const actual = validate('nika@yandex.ru');
        expect(actual).to.deep.equal(expected);
    });

    it('should return true when post service is outlook.com', () => {
        const expected = true;
        const actual = validate('nika@outlook.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return false when post service is yahoo.com', () => {
        const expected = false;
        const actual = validate('nika@yahoo.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return false when empty string', () => {
        const expected = false;
        const actual = validate('');
        expect(actual).to.deep.equal(expected);
    });

    it('should return false when wrong data', () => {
        const expected = false;
        const actual = validate('5hj37j');
        expect(actual).to.deep.equal(expected);
    });
});


describe('validateAsync function', () => {
    it('should return true asynchonously when post service is gmail.com', async function () {
        const expected = true;
        const actual = await validateAsync('nika@gmail.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return true asynchonously when post service is yandex.ru', async function () {
        const expected = true;
        const actual = await validateAsync('nika@yandex.ru');
        expect(actual).to.deep.equal(expected);
    });

    it('should return true asynchonously when post service is outlook.com', async function () {
        const expected = true;
        const actual = await validateAsync('nika@outlook.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return false asynchonously when post service is yahoo.com', async function () {
        const expected = false;
        const actual = await validateAsync('nika@yahoo.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return false asynchonously when empty string', async function () {
        const expected = false;
        const actual = await validateAsync('');
        expect(actual).to.deep.equal(expected);
    });

    it('should return false asynchonously when wrong data', async function () {
        const expected = false;
        const actual = await validateAsync('5hj37j');
        expect(actual).to.deep.equal(expected);
    });
});


describe('validateWithThrow', () => {
    it('should return true when post service is gmail.com', () => {
        const expected = true;
        const actual = validateWithThrow('nika@gmail.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should return true when post service is yandex.ru', () => {
        const expected = true;
        const actual = validateWithThrow('nika@yandex.ru');
        expect(actual).to.deep.equal(expected);
    });

    it('should return true when post service is outlook.com', () => {
        const expected = true;
        const actual = validateWithThrow('nika@outlook.com');
        expect(actual).to.deep.equal(expected);
    });

    it('should throw an error when post service is yahoo.com', () => {
        expect(() => { validateWithThrow('nika@yahoo.com') }).to.throw('the provided email is invalid');
    });

    it('should throw an error when empty string', () => {
        expect(() => { validateWithThrow('') }).to.throw('the provided email is invalid');
    });

    it('should throw an error when wrong data', () => {
        expect(() => { validateWithThrow('5hj37j') }).to.throw('the provided email is invalid');
    });
});


describe('validateWithLog', () => {

    it('should return true when post service is gmail.com', () => {
        let mock = sinon.mock(console);
        let expectation = mock.expects("log");
        const expected = true;
        const actual = validateWithLog('nika@gmail.com');
        expect(actual).to.deep.equal(expected);
        expectation.once();
        mock.restore();
        mock.verify();
    });

    it('should return true when post service is yandex.ru', () => {
        let mock = sinon.mock(console);
        let expectation = mock.expects("log");
        const expected = true;
        const actual = validateWithLog('nika@yandex.ru');
        expect(actual).to.deep.equal(expected);
        expectation.once();
        mock.restore();
        mock.verify();
    });

    it('should return true when post service is outlook.com', () => {
        let mock = sinon.mock(console);
        let expectation = mock.expects("log");
        const expected = true;
        const actual = validateWithLog('nika@outlook.com');
        expect(actual).to.deep.equal(expected);
        expectation.once();
        mock.restore();
        mock.verify();
    });

    it('should return false when post service is yahoo.com', () => {
        let mock = sinon.mock(console);
        let expectation = mock.expects("log");
        const expected = false;
        const actual = validateWithLog('nika@yahoo.com');
        expect(actual).to.deep.equal(expected);
        expectation.once();
        mock.restore();
        mock.verify();
    });

    it('should return false when empty string', () => {
        let mock = sinon.mock(console);
        let expectation = mock.expects("log");
        const expected = false;
        const actual = validateWithLog('');
        expect(actual).to.deep.equal(expected);
        expectation.once();
        mock.restore();
        mock.verify();
    });

    it('should return false when wrong data', () => {
        let mock = sinon.mock(console);
        let expectation = mock.expects("log");
        const expected = false;
        const actual = validateWithLog('5hj37j');
        expect(actual).to.deep.equal(expected);
        expectation.once();
        mock.restore();
        mock.verify();
    });

});

