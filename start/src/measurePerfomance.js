const { event } = require("jquery");

function measurePerformance(type, name, data, options = "") {
    console.log(`%c${type}: %c${name}: %c${Math.round(data)}  %c${options}`, "color: green", "color: maroon", "color: blue", "color: orange");
};


window.addEventListener("load", () => {
    let metrics = {};

    const navigationEntries = performance.getEntriesByType("navigation");
    navigationEntries.forEach(entry => {

        measurePerformance("navigation", "speed page load", entry.domContentLoadedEventEnd - entry.domContentLoadedEventStart, "ms");

        measurePerformance("navigation", "page memory", entry.transferSize, "b");

        metrics.pageLoad = Math.round(entry.domContentLoadedEventEnd - entry.domContentLoadedEventStart);
        metrics.pageMemory = Math.round(entry.transferSize);
    });

    const customObserver = new PerformanceObserver(list => {
        list.getEntries().forEach(entry => {
            measurePerformance(entry.entryType, entry.name,
                entry.duration, "ms");
            metrics.comFetch = Math.round(entry.duration);

        });
        postMetrics('http://localhost:3000/analytics/performance', { metrics })
            .then((data) => {
                console.log("confirm writing metrics", data);
                metrics = {};
            })
    });
    customObserver.observe({ entryTypes: ["measure"] });

});

async function postMetrics(url = '', data = {}) {
    const resp = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return await resp.json();
}

