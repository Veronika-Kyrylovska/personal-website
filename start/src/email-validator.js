module.exports.validate = function validate(email) {
    const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
    const check = email.split("@");
    if (VALID_EMAIL_ENDINGS.includes(check[1])) { return true; }
    else return false;
}
module.exports.validateAsync = async function validateAsync(email) {
    const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
    const check = email.split("@");
    if (VALID_EMAIL_ENDINGS.includes(check[1])) {
        return Promise.resolve(true);
    }
    else return Promise.resolve(false);
};
module.exports.validateWithThrow = function validateWithThrow(email) {
    const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
    const check = email.split("@");
    if (VALID_EMAIL_ENDINGS.includes(check[1])) {
        return true;
    }
    else {
        throw new Error('the provided email is invalid');
    };
}
module.exports.validateWithLog = function validateWithLog(email) {
    const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
    const check = email.split("@");
    if (VALID_EMAIL_ENDINGS.includes(check[1])) {
        console.log('true');
        return true;
    }
    else {
        console.log(false);
        return false;
    }
}
