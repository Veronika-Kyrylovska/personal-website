
self.addEventListener('sync', function (event) {
    if (event.tag.substring(0, 5) === 'umail') {
        const umail = event.tag.substring(5);
        console.log(umail);

        fetch('http://localhost:3000/subscribe', {

            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ "email": umail })
        })
            .then(response => response.json())

            .then(data => {
                console.log(data);
            })
            .catch(error => {
                console.log(error);
            })
    }
});

self.addEventListener('sync', function (event) {
    if (event.tag === 'unsubscribe') {
        console.log('unsubscribe');

        fetch('http://localhost:3000/unsubscribe', {

            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({})
        })
            .then(response => response.json())

            .then(data => {
                console.log(data);
            })
            .catch(error => {
                console.log(error);
            })
    }
});


