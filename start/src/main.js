import SectionCreatorPattern from './join-us-section.js';
import { validate } from "./email-validator.js";
import './styles/style.css';
import Community from "./Community.js";
import Worker from "./my.worker.js";
import { error } from 'jquery';

document.addEventListener('DOMContentLoaded', (event) => {
    const worker = new Worker();

    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/sw.js').then(registration => {
                console.log('SW registered: ', registration);
            }).catch(registrationError => {
                console.log('SW registration failed: ', registrationError);
            });
        });
    }


    const section = new SectionCreatorPattern();
    const appST = section.createProgram('standard');
    const sectCommunity = Community();
    const main = document.querySelector('main');
    const footer = document.querySelector('footer');
    const culture = document.querySelector(".app-section.app-section--image-culture");
    main.insertBefore(sectCommunity, culture);
    main.insertBefore(appST.joinUs(), footer);
    const formSBT = document.querySelector('form');
    const subBTN = document.getElementById('subButton');
    const mail = document.getElementById('subMail');
    const unsubBTN = document.getElementById('unsubButton');
    let respStatus;
    let isFetched = false;

    localStorage.getItem('email');
    mail.addEventListener('input', (event) => {
        localStorage.setItem('email', event.target.value);
        mail.value = localStorage.getItem('email');
        worker.postMessage({ data: 'enter email event' });
    });
    formSBT.addEventListener('submit', (event) => { // eslint-disable-line no-shadow
        event.preventDefault();
    });
    subBTN.addEventListener('click', () => {

        subBTN.disabled = true;
        subBTN.style.opacity = "0.5";
        localStorage.setItem('subscribing', 'is Subscribed');
        const email = localStorage.getItem('email');
        let resp = validate(email);
        if (resp) {
            worker.postMessage({ data: 'subscribe click event' });

            if ('SyncManager' in window) {
                navigator.serviceWorker.getRegistration()
                    .then(registration => {
                        registration.sync.register(`umail${email}`);


                        console.log('sending umail', email);
                        alert('You are successfully subscribed');
                        subBTN.disabled = false;
                        subBTN.style.opacity = "1";
                        mail.style.display = "none";
                        subBTN.style.display = "none";
                        unsubBTN.style.display = "block";
                    });
            }
            else {

                fetch('http://localhost:3000/subscribe', {

                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ "email": email })
                })
                    .then(response => response.json())

                    .then(data => {
                        if (data.error) {
                            window.alert(data.error);
                            throw Error();
                        }
                        else {
                            alert('You are successfully subscribed');
                            subBTN.disabled = false;
                            subBTN.style.opacity = "1";
                            mail.style.display = "none";
                            subBTN.style.display = "none";
                            unsubBTN.style.display = "block";
                        }
                    })
                    .catch(error => {
                        console.log(error);
                        localStorage.clear();
                        subBTN.disabled = false;
                        subBTN.style.opacity = "1";
                        mail.value = "";
                    })
            }
        }
    });

    unsubBTN.addEventListener('click', () => {
        worker.postMessage({ data: 'unsubscribe click event' });

        unsubBTN.disabled = true;
        unsubBTN.style.opacity = "0.5";

        if ('SyncManager' in window) {
            navigator.serviceWorker.getRegistration()
                .then(registration => {
                    registration.sync.register(`unsubscribe`);
                    console.log('sending unsubscribe');


                    alert('You are now unsubscribed');
                    unsubBTN.disabled = false;
                    unsubBTN.style.opacity = "1";
                    mail.style.display = "inline-block";
                    subBTN.style.display = "inline-block";
                    unsubBTN.style.display = "none";
                    mail.value = "";
                    localStorage.clear();
                })
        }
        else {

            fetch('http://localhost:3000/unsubscribe', {
                method: "POST",

                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({})
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    alert('You are now unsubscribed');
                    unsubBTN.disabled = false;
                    unsubBTN.style.opacity = "1";
                    mail.style.display = "inline-block";
                    subBTN.style.display = "inline-block";
                    unsubBTN.style.display = "none";
                    mail.value = "";
                    localStorage.clear();
                })
                .catch(error => window.alert(error))
        }
    });
});

