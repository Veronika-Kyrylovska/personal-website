class Program {
    constructor(headerText, str) {
        this.headerText = headerText;
        this.str = str;
    }
    addAppStyles(app) {
        app.style.height = "436px";
        app.style.width = "100%";
    }
    addCoverStyles(cover) {
        cover.style.background = "rgba(5, 17, 18, 0.5)";
        cover.style.height = "436px";
        cover.style.width = "100%";
    }
    addHeaderStyles(header) {
        header.style.color = "white";
        header.style.fontSize = "48px";
        header.style.lineHeight = "64px";
        header.style.fontWeight = "700";
        header.style.margin = "0";
        header.style.textAlign = "center";
    }
    addTextStyles(text) {
        text.style.color = "rgba(255, 255, 255, 0.7)";
        text.style.fontSize = "24px";
        text.style.lineHeight = "32px";
        text.style.marginBottom = "60px";
    }
    addFormStyles(form) {
        form.style.display = "flex";
        form.style.alignItems = "center";
        form.style.justifyContent = "center";
    }
    addMailStyles(mail) {
        mail.style.color = "white";
        mail.style.background = "rgba(255, 255, 255, 0.15)";
        mail.style.padding = "10px 0 10px 20px";
        mail.style.border = "none";
        mail.style.width = "380px";
        mail.style.height = "16px";
        mail.style.marginRight = "29px";
    }
    addSubBtnStyles(subBTN) {
        subBTN.style.background = "#55C2D8";
        subBTN.style.color = "white";
        subBTN.style.borderRadius = "18px";
        subBTN.style.fontSize = "14px";
        subBTN.style.lineHeight = "26px";
        subBTN.style.letterSpacing = "0.1em";
        subBTN.style.fontWeight = "400";
        subBTN.style.padding = "5.5px 17px";
        subBTN.style.border = "none";
        subBTN.style.display = "flex";
    }
    removeApp(app) { app.remove(); }
    joinUs() {
        const app = document.createElement("div");
        app.classList.add("app-section");
        app.setAttribute('id', 'appOnload');
        this.addAppStyles(app);
        const cover = document.createElement("div");
        cover.classList.add("app-section");
        this.addCoverStyles(cover);
        app.appendChild(cover);
        const header = document.createElement("h2");
        header.innerText = this.headerText;
        this.addHeaderStyles(header);
        cover.appendChild(header);
        const text = document.createElement("p");
        text.appendChild(document.createTextNode("Sed do eiusmod tempor incididunt"));
        const br = document.createElement("br");
        text.appendChild(br);
        text.appendChild(document.createTextNode("ut labore et dolore magna aliqua."));
        this.addTextStyles(text);
        cover.appendChild(text);
        const form = document.createElement("form");
        this.addFormStyles(form);
        const mail = document.createElement("input");
        mail.setAttribute('id', 'subMail');
        mail.setAttribute("placeholder", "E-mail");
        this.addMailStyles(mail);
        const subBTN = document.createElement("input");
        subBTN.setAttribute('id', 'subButton');
        subBTN.setAttribute("type", "submit");
        subBTN.setAttribute("value", this.str);
        this.addSubBtnStyles(subBTN);
        const unsubBTN = document.createElement("button");
        unsubBTN.setAttribute('id', 'unsubButton');
        unsubBTN.setAttribute("type", "submit");
        unsubBTN.textContent = "Unsubscribe";
        this.addSubBtnStyles(unsubBTN);
        if (localStorage.getItem('subscribing')) {
            mail.style.display = "none";
            subBTN.style.display = "none";
            unsubBTN.style.display = "block";
        }
        else {
            if (localStorage.getItem('email')) {
                mail.style.display = "inline-block";
                mail.value = localStorage.getItem('email');
                subBTN.style.display = "inline-block";
                unsubBTN.style.display = "none";
            }
            else {
                mail.style.display = "inline-block";
                subBTN.style.display = "inline-block";
                unsubBTN.style.display = "none";
                mail.value = localStorage.getItem('email');
            }
        }
        form.appendChild(mail);
        form.appendChild(subBTN);
        form.appendChild(unsubBTN);
        cover.appendChild(form);
        if (window.matchMedia("(max-width: 768px)").matches) {
            form.style.flexDirection = "column";
            subBTN.style.marginTop = "42px";
            app.style.height = "523px";
            cover.style.height = "523px";
            app.style.backgroundSize = "cover";
            app.style.backgroundPositionX = "-300px";
            mail.style.width = "300px";
        }
        return app;
    }
}
export default class SectionCreatorPattern {
    createProgram(type) {
        switch (type) { // eslint-disable-line default-case
            case 'standard': return new Program("Join Our Program", "SUBSCRIBE");
            case 'advanced': return new Program("Join Our Advanced Program", "Subscribe to Advanced Program");
        }
    }
}
