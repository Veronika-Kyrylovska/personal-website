let analyticsArray = [];

self.onmessage = function (event) {
    analyticsArray.push(event.data);

    if (analyticsArray.length === 5) {

        fetch('http://localhost:3000/analytics/user', {

            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ analyticsArray })
        })
            .then(response => response.json())

            .then(data => {
                console.log("confirm writing events", data);
                analyticsArray = [];
            });
    }
}

