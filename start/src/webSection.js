const template = document.createElement('template');
template.innerHTML = `
<style>

.app-title {
    font-size: 3.25rem;
    line-height: 3.20rem;
    text-align: center;
    font-family: 'Oswald', sans-serif;
  }
  
  .app-subtitle {
    font-size: 1.50rem;
    line-height: 1.625rem;
    font-weight: 300;
    text-align: center;
    font-family: 'Source Sans Pro', sans-serif, Arial;
  }
</style>

<div> <slot name="logo"></slot></div>

<h2 class="app-title"></h2>  

<div> <slot name="circle"></slot></div>

  <h3 class="app-subtitle"></h3>

  <article><slot name="article"/> </article>

  <div> <slot name="more"></slot></div>
`;

class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    if (!(this.hasAttribute('title') || this.hasAttribute('description'))) { console.error('Attributes "title" and "description" are obligatiry') };
    this.shadowRoot.querySelector('h2').innerHTML = this.getAttribute('title');
    this.shadowRoot.querySelector('h3').innerHTML = this.getAttribute('description');
  }

  connectedCallback() { }
  disconnectedCallback() { }
  adoptedCallback() { }
  attributeChangedCallback() { }

}
window.customElements.define('web-section', WebsiteSection);
