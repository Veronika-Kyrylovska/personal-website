export default function Community() {
    const sectionCommunity = document.createElement("div");
    sectionCommunity.classList.add("app-section");
    sectionCommunity.classList.add("community");
    const header = document.createElement("h2");
    header.classList.add("headersectionCommunity");
    header.appendChild(document.createTextNode("Big Community of"));
    const br = document.createElement("br");
    header.appendChild(br);
    header.appendChild(document.createTextNode("People Like You"));
    sectionCommunity.appendChild(header);
    const text = document.createElement("p");
    text.classList.add("textSectionCommunity");
    text.appendChild(document.createTextNode("We're proud of our products, and we're really excited"));
    const br1 = document.createElement("br");
    text.appendChild(br1);
    text.appendChild(document.createTextNode(" when we get feedback from our users."));
    sectionCommunity.appendChild(text);
    const sectionCards = document.createElement("section");
    sectionCards.classList.add("app-section__cards");
    async function getUsers() {
        try {
            performance.mark("getUsersStart");

            let res = await fetch('http://localhost:3000/community');

            performance.mark("getUsersEnd");
            performance.measure("Community", "getUsersStart", "getUsersEnd");

            return await res.json();
        }
        catch (error) { console.log(error); }
    }
    async function renderUsers() {
        let users = await getUsers();
        users.forEach(user => {
            const card = document.createElement("div");
            card.classList.add("userCard");
            const face = document.createElement("img");
            face.classList.add("userImage");
            face.setAttribute("src", `${user.avatar}`);
            face.setAttribute("alt", "photo of user");
            card.appendChild(face);
            const citate = document.createElement("p");
            citate.classList.add("userCitate");
            citate.textContent = `${user.citate}`;
            card.appendChild(citate);
            const name = document.createElement("h4");
            name.textContent = `${user.firstName}  ${user.lastName}`;
            card.appendChild(name);
            const position = document.createElement("h5");
            position.textContent = `${user.position}`;
            card.appendChild(position);
            sectionCards.appendChild(card);
        });
    }
    renderUsers();
    sectionCommunity.appendChild(sectionCards);
    return sectionCommunity;
}
