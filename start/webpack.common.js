const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: ['./src/main.js', './src/webSection.js', './src/measurePerfomance.js'],
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, "dist")
    },

    performance: {
        hints: false,
        maxEntrypointSize: 51200000,
        maxAssetSize: 51200000
    },
    plugins: [
        new HtmlWebpackPlugin(
            {
                template: 'src/index.html'
            }
        ),
        new CopyWebpackPlugin({
            patterns: [
                { from: "src/assets/images", to: "images" },
                { from: "src/scripts" }
            ]
        }),
        new CleanWebpackPlugin(),
    ],
    module: {
        rules: [
            // {
            //     enforce: 'pre',
            //     test: /\.js$/,
            //     exclude: /(node_modules)/,
            //     loader: 'eslint-loader'
            // },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/i,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                ]
            },
            {
                test: /\.(png|jpg)$/,
                use: [{
                    loader: 'url-loader',
                }]
            },
            {
                test: /\.worker\.js$/,
                use: { loader: "worker-loader" },
            }
        ]
    }
}