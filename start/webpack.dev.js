const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        hot: true,
        port: 5501,
        // proxy: [{ context: ['/community', '/subscribe', '/unsubscribe'], target: 'http://localhost:3000', }]
    },
});