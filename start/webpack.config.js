module.exports = process.env.NODE_ENV === 'dev' ? require('./webpack.dev') : require('./webpack.prod');
