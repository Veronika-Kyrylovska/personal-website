module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "airbnb-base"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "no-console": ["warn"],
        "no-nested-ternary": ["warn"],
        "no-mixed-operators": ["off"],
        "linebreak-style": ["off"],
        "no-underscore-dangle": ["off"],
        "class-methods-use-this": ["warn"],
        "no-useless-escape": ["error"],
        "no-unused-vars": ["warn"],
        "no-use-before-define": ["warn"],
        "no-delete-var": ["warn"],
        "indent": ["off"],
        "quotes": ["off"],
        "import/extensions": ["warn"],
        "no-param-reassign": ["warn"],
        "brace-style": ["off"],
        "padded-blocks": ["off"],
        "consistent-return": ["warn"],
        "operator-assignment": ["warn"],
        "no-restricted-syntax": ["warn"],
        "max-classes-per-file": ["off"],
        "no-unused-expressions": ["off"]
    }
};